import React, {Component} from "react";
import ModelDataService from "../services/models-service";
import {Link} from "react-router-dom";
import Pagination from "@material-ui/lab/Pagination";

export default class ModelList extends Component {
    constructor(props) {
        super(props);
        this.onChangeSearchName = this.onChangeSearchName.bind(this);
        this.retrieveModels = this.retrieveModels.bind(this);
        this.searchName = this.searchName.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
        this.handlePageSortChange = this.handlePageSortChange.bind(this);

        this.state = {
            models: [],
            currentModel: null,
            currentIndex: -1,
            searchName: "",

            page: 1,
            count: 0,
            pageSize: 20,

            sort: 'asc',
        };

        this.pageSort = ['asc', 'desc'];
    }

    componentDidMount() {
        this.retrieveModels();
    }

    onChangeSearchName(e) {

        const searchName = e.target.value.trim();

        this.setState({
            searchName: searchName
        });
    }

    getRequestParams(sort, page, pageSize) {
        let params = {};

        if (sort) {
            params["_order"] = sort;
        }

        if (page) {
            params["_page"] = page;
        }

        if (pageSize) {
            params["_limit"] = pageSize;
        }

        params["_sort"] = 'name';

        return params;
    }

    retrieveModels() {
        const {sort, page, pageSize} = this.state;
        const params = this.getRequestParams(sort, page, pageSize);

        ModelDataService.getAll(params)
            .then((response) => {

                this.setState({
                    models: response.data,
                    count: response.headers['x-total-count'],
                });
            })
            .catch((e) => {
                console.log(e);
            });
    }

    searchName() {
        if (!this.state.searchName) {
            return false;
        }

        ModelDataService.findByName(this.state.searchName)
            .then(response => {
                this.setState({
                    models: response.data,
                    count: response.headers['x-total-count'],
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    handlePageChange(event, value) {
        this.setState(
            {
                page: value,
            },
            () => {
                this.retrieveModels();
            }
        );
    }

    handlePageSortChange(e) {
        this.setState(
            {
                sort: e.target.value
            },
            () => {
                this.retrieveModels();
            }
        );
    }

    render() {
        const {
            searchName,
            models,
            page,
            count,
            pageSize,
            sort,
        } = this.state;

        return (
            <div className="list row">
                <h4>Models List</h4>

                <div className="col-md-12">


                    <div className="input-group mb-3">
                        <input
                            type="text"
                            className="form-control"
                            placeholder="Search by name"
                            value={searchName}
                            onChange={this.onChangeSearchName}
                        />
                        <div className="input-group-append">
                            <button
                                className="btn btn-outline-secondary"
                                type="button"
                                onClick={this.searchName}
                            >
                                Search
                            </button>
                        </div>
                    </div>


                    <div className="mt-3">
                        {"ORDER: "}
                        <select onChange={this.handlePageSortChange}>
                            {this.pageSort.map((item) => (
                                <option key={item} value={item} selected={(item === sort) ? 'selected' : ''}>
                                    {item}
                                </option>
                            ))}
                        </select>
                    </div>

                    <div className="d-flex justify-content-center">
                        {count ? (
                            <Pagination
                                className="my-3"
                                count={Math.round(count / pageSize)}
                                page={page}
                                siblingCount={1}
                                boundaryCount={1}
                                variant="outlined"
                                shape="rounded"
                                onChange={this.handlePageChange}
                            />
                        ) : (
                            <div>
                                <br/>
                            </div>
                        )}
                    </div>

                    <div className="row">

                        {models && models.map((model, index) => (
                            <div className={"col-4 border "} key={index}>

                                <h5>{model.name}</h5>

                                <div className={"row"}>
                                    <div className={"col-sm-6"}>
                                        <Link to={"/models/" + model.id + "/edit"} className="btn btn-success btn-sm">
                                            Edit
                                        </Link>
                                    </div>
                                    <div className={"col-sm-6"}>
                                        <Link to={"/models/" + model.id} className="btn btn-success btn-sm">
                                            View
                                        </Link>
                                    </div>
                                </div>

                                <img className={"img-fluid"} src={model.avatar} alt={model.name}/>
                            </div>
                        ))}
                    </div>

                </div>
            </div>
        );
    }
}