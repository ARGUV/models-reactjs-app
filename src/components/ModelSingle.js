import React, {Component} from "react";
import ModelDataService from "../services/models-service";

export default class ModelSingle extends Component {
    constructor(props) {
        super(props);
        this.getModel = this.getModel.bind(this);

        this.state = {
            currentModel: {
                id: null,
                name: "",
                avatar: "",
                age: "",
                eyes: "",
                gender: "",
                email: "",
                created_at: "",
                tags: [],
            },
            message: ""
        };
    }

    componentDidMount() {
        this.getModel(this.props.match.params.id);
    }

    getModel(id) {
        ModelDataService.get(id)
            .then(response => {
                this.setState({
                    currentModel: response.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const {currentModel} = this.state;

        return (
            <div>
                <div className="edit-form">
                    <h4>Single Model Page</h4>

                    <div className="form-group">
                        <img src={currentModel.avatar} alt={currentModel.name}/>
                    </div>

                    <div className="form-group">
                        <label>Name: </label>
                        {currentModel.name}
                    </div>

                    <div className="form-group">
                        <label>Gender: </label>
                        {currentModel.gender}
                    </div>

                    <div className="form-group">
                        <label>Age: </label>
                        {currentModel.age}
                    </div>

                    <div className="form-group">
                        <label>Eyes: </label>
                        {currentModel.eyes}
                    </div>

                    <div className="form-group">
                        <label>Email: </label>
                        {currentModel.email}
                    </div>

                    <div className="form-group">
                        <label>Created: </label>
                        {currentModel.created_at}
                    </div>

                    <div className="form-group">
                        <label>Tags: </label>
                        <ul>
                            {currentModel.tags.map(tag =>
                                <li key={tag}>{tag}</li>
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}