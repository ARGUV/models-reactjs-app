import React, {Component} from "react";
import ModelDataService from "../services/models-service";

export default class ModelUpdate extends Component {
    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.getModel = this.getModel.bind(this);
        this.updateModel = this.updateModel.bind(this);

        this.state = {
            currentModel: {
                id: null,
                name: "",
            },
            message: ""
        };
    }

    componentDidMount() {
        this.getModel(this.props.match.params.id);
    }

    onChangeName(e) {
        const name = e.target.value;

        this.setState(function (prevState) {
            return {
                currentModel: {
                    ...prevState.currentModel,
                    name: name
                }
            };
        });
    }

    getModel(id) {
        ModelDataService.get(id)
            .then(response => {
                this.setState({
                    currentModel: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    updateModel() {
        ModelDataService.update(
            this.state.currentModel.id,
            this.state.currentModel
        )
            .then(response => {
                this.setState({
                    message: "The Model was updated successfully!"
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const {currentModel} = this.state;

        return (
            <div>
                {currentModel ? (
                    <div className="edit-form">
                        <h4>Edit Model Page</h4>
                        <form>
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="name"
                                    value={currentModel.name}
                                    onChange={this.onChangeName}
                                />
                            </div>
                        </form>

                        <button
                            type="submit"
                            className="btn btn-success btn-sm"
                            onClick={this.updateModel}
                        >
                            Update
                        </button>

                        <p>{this.state.message}</p>
                    </div>
                ) : (
                    <div>
                        <br/>
                        <p>Model not exist</p>
                    </div>
                )}
            </div>
        );
    }
}