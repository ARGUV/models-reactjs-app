import http from "../http-common";

class ModelDataService {

    getAll(params) {
        return http.get("/models", {params});
    }

    get(id) {
        return http.get(`/models/${id}`);
    }

    update(id, data) {
        return http.put(`/models/${id}`, data);
    }

    findByName(name) {
        return http.get(`/models?q=${name}`);
    }
}

export default new ModelDataService();