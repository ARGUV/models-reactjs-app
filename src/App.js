import React, {Component} from "react";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import ModelUpdate from "./components/ModelUpdate";
import ModelList from "./components/ModelList";
import ModelSingle from "./components/ModelSingle";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <nav className="navbar navbar-expand navbar-dark bg-dark">
                        <div className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link to={"/"} className="nav-link">
                                    All Models
                                </Link>
                            </li>
                        </div>
                    </nav>

                    <div className="container mt-3">
                        <Switch>
                            <Route exact path={["/", "/models"]} component={ModelList}/>
                            <Route exact path="/models/:id" component={ModelSingle}/>
                            <Route path="/models/:id" component={ModelUpdate}/>
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;